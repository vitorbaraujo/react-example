import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableHighlight
} from 'react-native';

export default class TodoForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: ""
        }

        this.handleChangeText = this.handleChangeText.bind(this);
        this.handleAddButton = this.handleAddButton.bind(this);
    }

    handleChangeText(text) {
        this.setState({ text });
    }

    handleAddButton() {
        console.log(`Text: ${this.state.text}`);
        this.props.addTodoCallback(this.state.text);
        this.setState({text: ""});
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.input}>
                    <TextInput
                        onChangeText={this.handleChangeText}
                        value={this.state.text}
                    />
                </View>

                <TouchableHighlight style={styles.addButtonContainer} onPress={() => this.handleAddButton()}>
                    <View style={styles.addButton}>
                        <Text style={styles.addButtonText}>ADD</Text>
                    </View>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 5
    },

    input: {
        flex: 4
    },

    addButtonContainer: {
        flex: 1
    },

    addButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    },
});
