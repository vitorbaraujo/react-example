import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ListView
} from 'react-native';

import TodoItem from './todo-item';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class TodoList extends Component {
    constructor(props) {
        super(props);
            this.state = {
                dataSource: ds.cloneWithRows(props.todos)
            };
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);

        this.setState({
            dataSource: ds.cloneWithRows(nextProps.todos)
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) => <TodoItem text={rowData}/>}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 5
    },
});
