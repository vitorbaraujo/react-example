import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

export default class TodoItem extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.todoTextContainer}>
                    <Text>{this.props.text}</Text>
                </View>

                <View style={styles.removeButtonContainer}>
                    <Text style={{color: 'red', fontWeight: '800', fontSize: 26}}>X</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 60,
        flexDirection: 'row',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
        justifyContent: 'center',
    },

    todoTextContainer: {
        flex: 9
    },

    removeButtonContainer: {
        flex: 1,
        padding: 5,
        backgroundColor: '#999',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'red'
    }
});
