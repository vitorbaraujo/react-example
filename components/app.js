import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import TodoForm from './todo-form';
import TodoList from './todo-list';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            todos: []
        }

        this.handleNewTodo = this.handleNewTodo.bind(this);
    }

    handleNewTodo(text) {
        const todos = this.state.todos;
        todos.push(text);

        this.setState({ todos });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.todoForm}>
                    <TodoForm addTodoCallback={this.handleNewTodo}/>
                </View>

                <View style={styles.todoList}>
                    <TodoList todos={this.state.todos} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    todoForm :  {
        flex: 1 
    },

    todoList: {
        flex: 12
    }
});
